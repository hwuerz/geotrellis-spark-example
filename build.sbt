name := "geo-trellis-spark-example"

version := "0.1"

organization := "de.hwuerz.geotrellis"

scalaVersion := "2.11.12"

scalacOptions ++= Seq(
  "-deprecation",
  "-unchecked",
  "-Yinline-warnings",
  "-language:implicitConversions",
  "-language:reflectiveCalls",
  "-language:higherKinds",
  "-language:postfixOps",
  "-language:existentials")

fork := true
fork in run := true
javaOptions += "-Xmx1G"
publishMavenStyle := true
publishArtifact in Test := false
pomIncludeRepository := { _ => false }

resolvers += Resolver.mavenLocal

libraryDependencies ++= Seq(
//  "org.geotools" % "gt-referencing" % "2.6.4",

  // geotrellis and spark
  "org.locationtech.geotrellis" %% "geotrellis-spark" % "1.2.1",
  "org.locationtech.geotrellis" %% "geotrellis-util" % "1.2.1",

  "org.apache.spark"      %% "spark-core"       % "2.2.0",
  "org.scalatest"         %%  "scalatest"       % "2.2.0" % Test,

  "com.github.scopt" % "scopt_2.11" % "3.2.0"
)

// When creating fat jar, remote some files with
// bad signatures and resolve conflicts by taking the first
// versions of shared packaged types.
//assemblyMergeStrategy in assembly := {
//  case "reference.conf" => MergeStrategy.concat
//  case "application.conf" => MergeStrategy.concat
//  case "META-INF/MANIFEST.MF" => MergeStrategy.discard
//  case "META-INF\\MANIFEST.MF" => MergeStrategy.discard
//  case "META-INF/ECLIPSEF.RSA" => MergeStrategy.discard
//  case "META-INF/ECLIPSEF.SF" => MergeStrategy.discard
//  case _ => MergeStrategy.first
//}

initialCommands in console := """
 |import geotrellis.raster._
 |import geotrellis.vector._
 |import geotrellis.proj4._
 |import geotrellis.spark._
 |import geotrellis.spark.io._
 |import geotrellis.spark.io.hadoop._
 |import geotrellis.spark.tiling._
 |import geotrellis.spark.util._
 """.stripMargin
