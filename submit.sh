#!/bin/sh

# Build the application, create a jar package and submit the jar to spark.

# Pass all given arguments to the jar
appArgs="$@"

# Configure according to your setup
# The main class to launch
# mainClass="demo.HelloGeotrellis"
mainClass="geotrellisqmconvert.Main"

# The location of the packaged jar
jarLocation="target/scala-2.11/geo-trellis-spark-example_2.11-0.1.jar"

# sbt package has to be called first before we 
# can submit the jar
# echo "> Run: sbt package"
# sbt package

echo "Run > spark-submit"
spark-submit \
 --driver-memory 10g\
 --class $mainClass \
 --packages org.locationtech.geotrellis:geotrellis-spark_2.11:1.2.1,\
org.locationtech.geotrellis:geotrellis-util_2.11:1.2.1,\
com.github.scopt:scopt_2.11:3.2.0,\
 $jarLocation $appArgs
