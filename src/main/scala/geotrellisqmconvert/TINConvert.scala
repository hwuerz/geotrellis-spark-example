package geotrellisqmconvert

import qmconvert.Point3D
import geotrellis.raster.Tile
import qmconvert.{CornerPoints, Point3D}

/**
  * Contains methods to handle the conversion between Tiles, TINs and HPoints.
  */
object TINConvert {

  /**
    * The minimum height of a point.
    * If the height is not greater than this value, the point will be ignored.
    * Value is chosen as approximately pow(2, 15)
    */
  private val MIN_HEIGHT = -32760


  /**
    * Get the corner points of a tile. This uses the height at the corner positions and generates a CornerPoints object
    * based on them.
    * @param tile The tile whose corner points should be generated.
    * @param invertY Whether the y-coordinate is inverted.
    * @return A CornerPoints object for the passed tile.
    */
  def tileToCornerPoints(tile: Tile, invertY: Boolean): CornerPoints = {
    // The order is important
    val keys = List( (0, 0), (tile.cols - 1, 0), (0, tile.rows - 1), (tile.cols - 1, tile.rows - 1) )
    val cornerPoints = keys.map(key => {
      var height = tile.get(key._1, key._2)
      if (height < MIN_HEIGHT) {
        height = CornerPoints.DEFAULT_HEIGHT
      }
      val y = if(invertY) tile.rows - 1 - key._2 else key._2
      new Point3D(key._1, y, height)
    })
    new CornerPoints(cornerPoints)
  }

}
