package geotrellisqmconvert

import geotrellis.raster.TileLayout
import geotrellis.spark.tiling.LayoutDefinition
import geotrellis.vector.Extent

object CesiumLayoutScheme {
  val DEFAULT_TILE_SIZE = 256

  def layoutColsForZoom(level: Int): Int = math.pow(2, level + 1).toInt
  def layoutRowsForZoom(level: Int): Int = math.pow(2, level).toInt

  def layoutForZoom(zoom: Int, layoutExtent: Extent, tileSize: Int = DEFAULT_TILE_SIZE): LayoutDefinition = {
    if(zoom < 0)
      sys.error("TMS Tiling scheme does not have levels below 0")

    LayoutDefinition(layoutExtent, TileLayout(layoutColsForZoom(zoom), layoutRowsForZoom(zoom), tileSize, tileSize))
  }
}
