package geotrellisqmconvert

import geotrellisqmconvert.SubTileDirection.{BottomLeft, BottomRight, TopLeft, TopRight}
import geotrellis.spark.SpatialKey
import geotrellis.util._
import org.apache.spark.rdd.RDD

import scala.collection.mutable.ListBuffer

object GroupLayerDown {

  /**
    * Groups all tiles of a given rdd according to the spatial keys of the lower zoom level.
    *
    * @param rdd the rdd which contains data on a specific zoom level
    * @tparam V the value associated with the spatial key
    * @return the rdd which contains a list buffer for every key of the lower zoom level containing the corresponding
    *         original tiles from the higher zoom level (the input rdd)
    */
  def apply[V](rdd: RDD[(SpatialKey, V)]): RDD[(SpatialKey, ListBuffer[(SubTileDirection, (SpatialKey, V))])] = {
    val neighbored: RDD[(SpatialKey, (SubTileDirection, (SpatialKey, V)))] =
      rdd
        .map { case (key, value) =>
          val SpatialKey(col, row) = key

          // assign the (key, value) pairs to to the corresponding key in the upper layer
          if (col % 2 == 0 && row % 2 == 0) {
            (key.setComponent(SpatialKey(col / 2, row / 2)), (TopLeft, (key, value)))
          }
          else if (col % 2 == 1 && row % 2 == 0) {
            (key.setComponent(SpatialKey((col - 1) / 2, row / 2)), (TopRight, (key, value)))
          }
          else if (col % 2 == 0 && row % 2 == 1) {
            (key.setComponent(SpatialKey(col / 2, (row - 1) / 2)), (BottomLeft, (key, value)))
          }
          else { // if (col % 2 == 1 && row % 2 == 1)
            (key.setComponent(SpatialKey((col - 1) / 2, (row - 1) / 2)), (BottomRight, (key, value)))
          }
        }

    def emptyList = scala.collection.mutable.ListBuffer.empty[(SubTileDirection, (SpatialKey, V))]

    val grouped: RDD[(SpatialKey, ListBuffer[(SubTileDirection, (SpatialKey, V))])] =
      rdd.partitioner match {
        case Some(partitioner) => neighbored.aggregateByKey(emptyList, partitioner)((it, elem) => it += elem, (it1, it2) => it1 ++= it2)
        case None => neighbored.aggregateByKey(emptyList)((it, elem) => it += elem, (it1, it2) => it1 ++= it2)
      }

    grouped
  }
}