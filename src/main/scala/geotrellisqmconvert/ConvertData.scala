package geotrellisqmconvert

import java.io.File

import geotrellisqmconvert.GenerateLayers.setJobTitle
import geotrellis.proj4.CRS
import geotrellis.raster.Tile
import geotrellis.spark._
import geotrellis.spark.io.hadoop.HadoopGeoTiffRDD
import geotrellis.vector.{Extent, ProjectedExtent}
import org.apache.hadoop.fs.Path
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object ConvertData {

  val WGS84: CRS = geotrellis.proj4.CRS.fromName("EPSG:4326")
  val frankfurtExtent = Extent(8.648197, 50.100229, 8.691613, 50.116996)
  val frankfurtFilter = Some(ProjectedExtent(frankfurtExtent, WGS84))

  def printSparkInfo(sc: SparkContext) : Unit = {
    println("Spark Config:")

    val conf = sc.getConf.getAll
    conf.foreach(keyVal => println(s" > ${keyVal._1}: ${keyVal._2}"))

    val url = sc.uiWebUrl match {
      case Some(uri) => uri
      case None => "Not available"
    }
    println(s"Web URL: $url")
  }

  def apply(dataLocation: File, bottomLayer: Int, topLayer: Int, cesiumTerrainDir: File,
                            filter: Option[ProjectedExtent], doClearTerrainDir: Boolean, sparkMaster: String,
                            verbose: Boolean, partitionBytes: Option[Long], logging: Boolean): Unit = {

    // Prepare Spark.
    val conf = new SparkConf().setAppName("QMConvert").setMaster(sparkMaster)
      //.set("spark.driver.memory", "10g")
      .set("spark.task.maxFailures", "1")
      .set("spark.executor.memory", "700m")
      .set("spark.memory.fraction", "0.2")
      .set("spark.executor.cores", "2")
      .set("spark.executor.instances", "3")
    implicit val sc: SparkContext = new SparkContext(conf)

    // print debug info
    println("Max Memory: " + (Runtime.getRuntime.maxMemory / (1000 * 1000)) + "m")
    printSparkInfo(sc)

    setJobTitle("Loading the data set(s)")

    val options = HadoopGeoTiffRDD.Options(partitionBytes = partitionBytes)

    // load the data and repartition it when partitionFactor contains a value which is not None
    val dataSet: RDD[(ProjectedExtent, Tile)] = {
      // first load the base data set(s)
      val baseDataSet = HadoopGeoTiffRDD.spatial(new Path(dataLocation.getAbsolutePath), options)
        // The tiling expects wgs84 data --> Ensure this format
        .reproject(WGS84)

      baseDataSet.setName("Projected base data set")

      // filter the dataSet according to the specified extent
      filter match {
        case Some(filterExtent) =>
          baseDataSet.flatMap(pair => {
            // reprojection can be removed when we use a specific projection later
            val tileExtent = pair._1.reproject(filterExtent.crs)
            tileExtent.intersection(filterExtent.extent) match {
              case Some(intersection) =>
                val intersectionProjExtent = ProjectedExtent(intersection.reproject(filterExtent.crs, pair._1.crs), pair._1.crs)
                val intersectionTile = pair._2.crop(tileExtent, intersection)
                Some((intersectionProjExtent, intersectionTile))

              case None =>
                None
            }
          })

        case None =>
          baseDataSet
      }
    }
    dataSet.setName("Data set")
    println(s"\rLoading dataset with ${dataSet.partitions.length} partitions.")

    setJobTitle("Starting terrain generation")
    try {
      GenerateLayers(dataSet, bottomLayer, topLayer, cesiumTerrainDir.getAbsolutePath, verbose)

      // wait for any key to be pressed before we exit so that the local spark context is still alive
      println("\rPress return to exit.")
      scala.io.StdIn.readLine()
    }
    catch {
      case e: UnsupportedOperationException =>
        if(e.getMessage.equals("empty collection")) {
          println("ERROR: The data set is empty! Maybe you provided a wrong filter.")
        }
        else
          e.printStackTrace()

      case e: Throwable => e.printStackTrace()
    }
  }
}
