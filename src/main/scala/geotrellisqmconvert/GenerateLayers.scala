package geotrellisqmconvert

import java.io._
import java.util

import geotrellisqmconvert.SubTileDirection.{BottomLeft, BottomRight, TopLeft, TopRight}
import geotrellisqmconvert.TINConvert._
import qmconvert.CornerPoints
import geotrellis.raster.Tile
import geotrellis.spark.tiling._
import geotrellis.spark.{SpatialKey, TileLayerMetadata, _}
import geotrellis.vector.ProjectedExtent
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.storage.StorageLevel
import qmconvert.{CornerPoints, QuantizedMeshTile}
import spire.syntax.cfor.cfor

import scala.collection.JavaConversions._

object GenerateLayers {

  def printProgress(str: String) : Unit = {
    println("\rGeneration > " + str)
  }

  /**
    * Collects the corner points from the tiles below. If there is no tile on one position, new corner points will be
    * created.
    * @param pair The pair with the lower tiles and their keys.
    * @param layout The used layout for tiling.
    * @param invertY Whether the y-coordinate is inverted in the sub-tiles.
    * @return The corner points for the higher tile.
    */
  def mergeCornerPointsForHigherLevel(pair: (SpatialKey, scala.Iterable[(SubTileDirection, (SpatialKey, QuantizedMeshTile))]),
                                      layout: LayoutDefinition,
                                      invertY: Boolean) : CornerPoints = {

    // println(s"Subtiles for ${pair._1}: ${pair._2.foldRight(" ")((dirKeyPair, str) => s"$str ${dirKeyPair._2._1}x${dirKeyPair._1}")}")

    // There has to be one point for each direction.
    // The order of these points is important. See CornerPoints constructor below.
    val cornerPoints = util.Arrays.asList(TopLeft, TopRight, BottomLeft, BottomRight)
      .map(direction => {
        // Find the correct sub-tile for the current direction.
        val subTile = pair._2.find(pair2 => {
          pair2._1.equals(direction)
        })
        if(subTile.isDefined) { // The sub tile for the current direction exists --> Reuse the corner points.
          val subTileDirection = subTile.get._1
          val qmTile = subTile.get._2._2
          val cornerPoints = qmTile.cornerPoints.get(subTileDirection)
          // println(s"Subtile defined for ${pair._1}->$direction (using dir: $subTileDirection) with cornerpoints: $cornerPoints")
          cornerPoints
        } else {
          // There is no sub tile in the current direction
          // --> Create a new corner point (in the correct direction, therefore never inverted).
          val cornerPoint = new CornerPoints(layout, invertY).get(direction)
          // println(s"Subtile not defined for ${pair._1}->$direction with cornerpoints: $cornerPoint")
          cornerPoint
        }
      })
      .toList

      new CornerPoints(cornerPoints)
  }

  def printStats(bottomLayer: Int, durations: util.List[Long], totalDuration: Long, initializationDuration: Long): Unit = {
    def msToMultiTimeFormatString(duration: Long): String = {
      val msPerMinute: Long = 60 * 1000
      val msPerHour: Long = 60 * msPerMinute

      var remainingDuration = duration

      val hours: Long = remainingDuration / msPerHour
      remainingDuration -= hours * msPerHour

      val minutes = remainingDuration / msPerMinute
      remainingDuration -= minutes * msPerMinute

      val seconds = remainingDuration / 1000
      val ms = duration % 1000

      s"$hours h\t$minutes min\t$seconds sec\t$ms ms"
    }

    printProgress("Timing statistics per layer:")

    printProgress(s"Initialization: \t${msToMultiTimeFormatString(initializationDuration)}")
    for (i <- 0 until durations.size) {
      printProgress(s"- Layer ${bottomLayer - i}: \t${msToMultiTimeFormatString(durations(i))}")
    }
    printProgress("------------------------------------------------")
    printProgress(s"Total duration: ${msToMultiTimeFormatString(totalDuration)}")
  }

  /**
    * Set the title of the job in the spark UI for every following job.
    * @param title The title (null for default)
    * @param sc The corresponding spark context
    */
  def setJobTitle(title: String)(implicit sc: SparkContext): Unit = {
    // sc.setLocalProperty("callSite.short", title)
    sc.setJobDescription(title)
  }

  /**
    * Generate the terrain files for the layers from zoom level bottomLayer to topLayer.
    *
    * @param dataSet The rdd which contains the rater data for the conversion
    * @param bottomLayer The bottom layer for which the terrain files should be generated (bottomLayer > topLayer)
    * @param topLayer The top layer for which the terrain files should be generated (bottomLayer > topLayer)
    * @param rootSavePath The root save path for the tile data
    * @param verbose Whether to output verbose information like the currently converted tiles
    * @param invertY Whether the y-Coordinate of each tile should be inverted (defaults to true)
    * @param maxPoints The maximum amount of points which can be used to triangulate the points (defaults to -1 for unrestricted)
    */
  def apply(dataSet: RDD[(ProjectedExtent, Tile)], bottomLayer: Int, topLayer: Int, rootSavePath: String, verbose: Boolean, invertY: Boolean = true, maxPoints: Int = -1, name: String = "terrain"): Unit = {
    if(bottomLayer < topLayer) {
      throw new IllegalArgumentException("The bottom zoom level must be greater or equal to the top layer of the pyramid!")
    }
    implicit val sparkContext: SparkContext = dataSet.sparkContext
    val startTime = System.currentTimeMillis()

    printProgress(s"Created the base terrain files of layer 0.")
    printProgress(s"Starting the conversion from layer $bottomLayer to $topLayer.")

    setJobTitle(s"Convert dataset to tiles for layer $bottomLayer")

    // convert the dataSet to the tile layout of the bottom zoom layer
    val extent = geotrellis.proj4.CRS.fromName("EPSG:4326").worldExtent
    val layout = CesiumLayoutScheme.layoutForZoom(bottomLayer, extent)
    val metadata: TileLayerMetadata[SpatialKey] = dataSet.collectMetadata[SpatialKey](layout)
    val rdd = ContextRDD(dataSet.tileToLayout[SpatialKey](metadata), metadata)

    val rootSaveDir = new File(rootSavePath)

    printProgress(s"Converting the bottom layer ($bottomLayer) to TINs.")
    val firstLayerStartTime = System.currentTimeMillis()
    setJobTitle(s"Convert bottom layer ($bottomLayer) to QMTs")
    val bottomTilesProgress = sparkContext.longAccumulator(s"Converted bottom layer ($bottomLayer) tiles")

    // convert the first layer of the dataset to TIN and save the tins
    val firstLayer = rdd.map(pair => {
      val spatialKey = pair._1
      val tile = pair._2

      // Just print the data as a simulation for real work.
      tile.foreach( (x, y, z) => {println(x + " | " + y + " --> " + z)} )
      val cornerPoints = tileToCornerPoints(tile, invertY)
      val qmTile = new QuantizedMeshTile(cornerPoints, spatialKey, bottomLayer, layout, invertY)

      // Save the Quantized Mesh on the file system.
      qmTile.write(rootSaveDir)

      if(verbose)
        printProgress(s"Done converting ($bottomLayer, ${spatialKey.row}, ${spatialKey.col})")

      bottomTilesProgress.add(1)

      // return the new layer which consists of the tins
      (spatialKey, qmTile)
    })

    firstLayer.setName(s"Bottom layer ($bottomLayer)")
    firstLayer.persist(StorageLevel.MEMORY_AND_DISK)
    // block until converting the first layer is done
    firstLayer.foreach(pair => ())

    val firstLayerTime = System.currentTimeMillis() - firstLayerStartTime
    val layerDurations: util.LinkedList[Long] = new util.LinkedList[Long]()
    layerDurations.addLast(firstLayerTime)

    printProgress(s"Finished converting the bottom layer ($bottomLayer) after ${firstLayerTime / 1000} seconds.")

    if (bottomLayer > topLayer)
      printProgress(s"Starting the conversion of the inner layers ${bottomLayer - 1} to $topLayer.")

    var currentLayer = firstLayer
    // convert the inner layers (from i = (bottomLayer - 1) to i = topLayer)
    cfor(bottomLayer - 1)(_ >= topLayer, _ - 1) { zoom =>
      val layerStartTime = System.currentTimeMillis()

      setJobTitle(s"Aggregate inner layer $zoom")

      val nextLayerIterable = GroupLayerDown(currentLayer)
      nextLayerIterable.setName(s"Aggregated tiles for layer $zoom")

      // println(s"Got next layer keys: ${nextLayerIterable.collect().foldLeft[String](" ")((str, pair) => str + pair._1.toString)}")
      val innerTilesProgress = sparkContext.longAccumulator(s"Converted inner layer ($zoom) tiles")

      currentLayer = nextLayerIterable.map(pair => {
        val spatialKey = pair._1
        val cornerPoints: CornerPoints = mergeCornerPointsForHigherLevel(pair, layout, invertY)

          // Generate the new tile for the current position.
          val qmTile = new QuantizedMeshTile(cornerPoints, spatialKey, zoom, layout, invertY)

          // Write the results to the file system.
          qmTile.write(rootSaveDir)

          if(verbose)
            printProgress(s"Done converting ($zoom, ${spatialKey.row}, ${spatialKey.col})")

          innerTilesProgress.add(1)

          // The new RDD (the level above) will use this qmTile for its data.
          (spatialKey, qmTile)
      })

      currentLayer.setName(s"Inner layer ($zoom)")
      currentLayer.persist(StorageLevel.MEMORY_AND_DISK)
      currentLayer.foreach(pair => ())

      val time = System.currentTimeMillis() - layerStartTime
      layerDurations.addLast(time)

      printProgress(s"Finished converting layer $zoom after ${time / 1000} seconds.")
    }

    if(bottomLayer == topLayer)
      // This block is required to add a reading access on the last RDD.
      // Without this, spark thinks the data is not needed and does not perform any calculations.
      // Only relevant when we did not convert any intermediate layers
      currentLayer.foreach(pair => ())

    // reset to default job title
    setJobTitle(null)

    printProgress(s"Finished generating the terrain files.")
    printStats(bottomLayer, layerDurations, System.currentTimeMillis() - startTime, firstLayerStartTime - startTime)
  }
}
