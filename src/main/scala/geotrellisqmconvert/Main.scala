package geotrellisqmconvert

import java.io.File
import geotrellis.vector.ProjectedExtent

object Main {
  case class Config(dataLocation: File = new File("."), bottomLayer: Int = -1, topLayer: Int = -1,
                    cesiumTerrainDir: File = new File("."), clearTerrainDir: Boolean = false,
                    sparkMaster: String = "local[*]", partitionBytes: Long = -1, verbose: Boolean = false,
                    logging: Boolean = false)

  def main(args: Array[String]): Unit = {
    val parser = new scopt.OptionParser[Config]("scopt") {
      head("qmconvert")

      opt[File]('d', "dataLocation").required().valueName("<file>").
        validate(f =>
          if (f.exists()) success
          else failure("The specified dataLocation does not exist!")
        ).
        action( (x, c) => c.copy(dataLocation = x) ).
        text("The location (single file or directory) which contains the .tif files which should be triangulated to terrain data.")

      opt[Int]('b', "bottomLayer").required().action( (x, c) =>
        c.copy(bottomLayer = x) ).text("The bottom layer of terrain data which will be generated.")

      opt[Int]('t', "topLayer").required().action( (x, c) =>
        c.copy(topLayer = x) ).text("The top layer of terrain data which will be generated.")

      opt[File]('c', "cesiumTerrainDir").required().valueName("<dir>").
        action( (x, c) => c.copy(cesiumTerrainDir = x) ).
        validate( x  =>
          if (x.isDirectory) success
          else failure("The value for cesiumTerrainDir must be a directory!")
        ).
        text("The directory where the generated terrain file will be written.")

      opt[Boolean]('e', "clearTerrainDir").action( (x, c) =>
        c.copy(clearTerrainDir = x) ).
        text("Clear (erase) the terrain directory before generating the new terrain files. Default: false.")

      opt[String]('s', "sparkMaster").action( (x, c) =>
        c.copy(sparkMaster = x) ).
        text("Define the url of the spark master which should be used for the execution. Default: local[*].")

      opt[Long]("partitionBytes").action( (x, c) =>
        c.copy(partitionBytes = x) ).
        text("The number of bytes of the original dataset(s) which will be used for a partition on the bottom layer.")

      opt[Boolean]("logging").action( (x, c) =>
        c.copy(logging = x) ).
        text("Whether to enable/disable logging. Default: false.")

      opt[Unit]("verbose").action( (_, c) =>
        c.copy(verbose = true) ).text("Print verbose information about the conversion process. Default: false.")

      help("help").text("prints this usage text")

      checkConfig( c =>
        // only run these checks when the bottom and the top layer are actually specified
        if(c.bottomLayer != -1 && c.topLayer != -1) {
          if (c.topLayer > c.bottomLayer)
            failure("The top layer must be smaller or equal to the top layer zoom!")
          else if (c.topLayer < 0)
            failure(s"The smallest allowed top level is 0. Given: ${c.topLayer}.")
          else success
        }
        else success
      )
    }

    parser.parse(args, Config()) match {
      case Some(config) =>
        val filter: Option[ProjectedExtent] = None
        val partitionBytes: Option[Long] = if (config.partitionBytes == -1) None else Some(config.partitionBytes)

        ConvertData(config.dataLocation, config.bottomLayer, config.topLayer, config.cesiumTerrainDir, filter,
          config.clearTerrainDir, config.sparkMaster, config.verbose, partitionBytes, config.logging)
      case None =>
        // happens when the arguments were not okay
    }
  }
}
