package geotrellisqmconvert

sealed trait SubTileDirection

object SubTileDirection {
  case object TopRight extends SubTileDirection
  case object BottomRight extends SubTileDirection
  case object BottomLeft extends SubTileDirection
  case object TopLeft extends SubTileDirection
}
