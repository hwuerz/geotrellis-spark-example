package demo

import java.io.File

import geotrellis.raster.Tile
import geotrellis.spark.tiling.LayoutDefinition
import geotrellisqmconvert.ConvertData
import geotrellis.vector.ProjectedExtent
import org.apache.spark.rdd.RDD
import geotrellis.spark.SpatialKey
import geotrellis.spark._

object HelloGeotrellis {

  def main(args: Array[String]): Unit = {
    generateExample("../cesium/terrain")
  }

  def generateExample(baseDir: String): Unit = {
    val threads: Int = 2
    val dataLocation: File = new File("DebugFiles/example.tif")
    val bottomLayer = 20
    val topLayer = 8
    val cesiumTerrainDir: File = new File(baseDir)
    val filter: Option[ProjectedExtent] = None//ConvertData.frankfurtFilter
    val doClearTerrainDir: Boolean = true // Ignored in this example
    val sparkMaster: String = s"local[$threads]"
    val verbose: Boolean = true
    val partitionBytes: Option[Long] = None
    val logging: Boolean = true // Ignored in this example

    ConvertData(dataLocation, bottomLayer, topLayer, cesiumTerrainDir, filter, doClearTerrainDir,
      sparkMaster, verbose, partitionBytes, logging)
  }

  def getKeysRDD(rdd: RDD[(ProjectedExtent, Tile)], layoutDefinition: LayoutDefinition): RDD[SpatialKey] = {
    val mapTransform = layoutDefinition.mapTransform

    rdd.flatMap[SpatialKey] { tup =>
      val (inKey, tile) = tup
      val extent = inKey.extent

      mapTransform(extent)
        .coordsIter
        .map { spatialComponent =>
          inKey.translate(spatialComponent)
        }
    }
  }
}
