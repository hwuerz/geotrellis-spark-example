package demo

import geotrellis.proj4.CRS
import geotrellis.spark.{ContextRDD, SpatialKey, TileLayerMetadata}
import geotrellis.spark.io.hadoop.HadoopGeoTiffRDD
import geotrellisqmconvert.CesiumLayoutScheme
import org.apache.hadoop.fs.Path
import geotrellis.spark._
import geotrellis.raster._
import geotrellis.spark.tiling._
import org.apache.spark.rdd._
import geotrellis.vector._
import org.apache.spark.{SparkConf, SparkContext}

object MinimalExample {

  private val WGS84: CRS = geotrellis.proj4.CRS.fromName("EPSG:4326")
  private val worldExtent = WGS84.worldExtent

  def main(args: Array[String]) : Unit = {
    // whether we trigger a heap error
    val heapError: Boolean = true

    // prepare spark
    val conf = new SparkConf().setAppName("Example").setIfMissing("spark.master", "local[2]")
    implicit val sc: SparkContext = new SparkContext(conf)

    // define our data (may be a directory containing many large tif files)
    val pathString = "DebugFiles/example.tif"
    // and the zoom level we want to handle
    val zoom = 18

    // load the data from our .tif files to a rdd
    val optionsWorking = HadoopGeoTiffRDD.Options(maxTileSize = Some(10))
    val optionsNotWorking= HadoopGeoTiffRDD.Options()
    val options = if (heapError) optionsNotWorking else optionsWorking

    val dataSet: RDD[(ProjectedExtent, Tile)] =
      HadoopGeoTiffRDD.spatial(new Path(pathString), options).reproject(WGS84) match {
        case data if heapError => data
        case data if !heapError => data.repartition(10)
      }

    // convert the data to fit the tiles according to the layout definition at that zoom level
    val layout: LayoutDefinition = CesiumLayoutScheme.layoutForZoom(zoom, worldExtent)
    val metadata: TileLayerMetadata[SpatialKey] = dataSet.collectMetadata[SpatialKey](layout)
    val rdd = ContextRDD(dataSet.tileToLayout[SpatialKey](metadata), metadata)

    // trigger work
    println(s"Count: ${rdd.count()}")
  }
}
