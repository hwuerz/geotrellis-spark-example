package qmconvert

import geotrellisqmconvert.SubTileDirection
import geotrellisqmconvert.SubTileDirection.{BottomLeft, BottomRight, TopLeft, TopRight}
import geotrellis.spark.tiling.LayoutDefinition

/**
  * Wrapper for the corner points of a tile. The corner points are required for triangulation and define the four
  * corners of a tile.
  * @param topLeft The top left corner.
  * @param topRight The top right corner.
  * @param bottomLeft The bottom left corner.
  * @param bottomRight The bottom right corner.
  */
case class CornerPoints(private val topLeft: Point3D,
                   private val topRight: Point3D,
                   private val bottomLeft: Point3D,
                   private val bottomRight: Point3D) extends Serializable {

  /**
    * Generate default corner points based on the passed layout definition.
    * @param layout The used layout definition to describe a tile.
    * @param invertY Whether the y-Coordinate of each tile should be inverted.
    */
  def this(layout: LayoutDefinition, invertY: Boolean) {
    this(new Point3D(0, CornerPoints.getTopYCoordinate(layout, invertY), CornerPoints.DEFAULT_HEIGHT),
      new Point3D(layout.tileCols - 1, CornerPoints.getTopYCoordinate(layout, invertY), CornerPoints.DEFAULT_HEIGHT),
      new Point3D(0, CornerPoints.getBottomYCoordinate(layout, invertY), CornerPoints.DEFAULT_HEIGHT),
      new Point3D(layout.tileCols - 1, CornerPoints.getBottomYCoordinate(layout, invertY), CornerPoints.DEFAULT_HEIGHT))
  }

  /**
    * Generate corner points based on a list. The points have to be passed in the order top left, top right, bottom
    * left, bottom right.
    * @param points The list of points in the correct order.
    */
  def this(points: List[Point3D]) {
    this(points.head, points(1), points(2), points(3))
  }

  /**
    * Get one corner points based on the passed direction.
    * @param direction The direction which selects the corner point.
    * @return The corner point in the requested direction.
    */
  def get(direction: SubTileDirection): Point3D = {
    direction match {
      case TopLeft => topLeft
      case TopRight => topRight
      case BottomLeft => bottomLeft
      case BottomRight => bottomRight
      case _ =>
        println("Warning: Unsupported CornerPoint in direction " + direction)
        null
    }
  }

  /**
    * Checks whether the passed point is on the same position as a corner point.
    * Only compares the x and y direction. This means, the function returns true if the passed
    * point is exactly above or below a corner point.
    * @param point The point to be checked whether it is on a corner.
    * @return True if the passed point is on a corner position, false otherwise.
    */
  def isOnCorner(point: Point3D): Boolean =
    toList.exists(corner => corner.x == point.x && corner.y == point.y)

  /**
    * Collect all corner points in a list.
    * @return A list of all corner points.
    */
  def toList: List[Point3D] = {
    List(topLeft, topRight, bottomLeft, bottomRight)
  }

  override def toString: String = {
    toList.toString()
  }
}

/**
  * Some static helper functions for corner point translation.
  * HPoints can not be serialized, but this is required for spark.
  * So they are stored as Point3D and converted for access.
  */
object CornerPoints {

  /**
    * The height of a corner point if no custom data was set.
    */
  val DEFAULT_HEIGHT: Int = 0

  /**
    * Get the y coordinate for the top border of a tile with the passed layout.
    * @param layout The layout of the tile.
    * @param invertY Whether the y-coordinate should be inverted (bottom-left on 0|0) or not (top-left on 0|0).
    * @return The y-coordinate of the top border.
    */
  def getTopYCoordinate(layout: LayoutDefinition, invertY: Boolean): Int = {
    if (invertY) {
      layout.tileRows - 1
    } else {
      0
    }
  }

  /**
    * Get the y coordinate for the bottom border of a tile with the passed layout.
    * @param layout The layout of the tile.
    * @param invertY Whether the y-coordinate should be inverted (bottom-left on 0|0) or not (top-left on 0|0).
    * @return The y-coordinate of the bottom border.
    */
  def getBottomYCoordinate(layout: LayoutDefinition, invertY: Boolean): Int = {
    getTopYCoordinate(layout, !invertY)
  }

}
