package qmconvert

import java.io.File

import geotrellis.spark.SpatialKey
import geotrellis.spark.tiling.LayoutDefinition
import geotrellis.vector.Extent

/**
  * This is an adjusted version of the original de.geo.toolbox.qmconvert.QuantizedMeshTile class written in java.
  * It organizes one tile in a quantized mesh.
  */
class QuantizedMeshTile(private val extent: Extent,
                        private val zoomLevel: Int,
                        private val positionX: Int,
                        private val positionY: Int,
                        val cornerPoints: CornerPoints) extends Serializable {

  /**
    * Generate a QuantizedMeshTile from spark results.
    * @param cornerPoints The corner points of this tile.
    * @param spatialKey The key of this tile. Will be used to define the file system path for writing.
    * @param zoom The current zoom level. Will be used to define the file system path for writing.
    * @param layout The used layout to generate this tile. Used to calculate the extent of the tile.
    * @param invertY Whether the y-coordinate from the spatial key has to be inverted before writing to the file system.
    */
  def this(cornerPoints: CornerPoints, spatialKey: SpatialKey, zoom: Int, layout: LayoutDefinition, invertY: Boolean) {
    this(spatialKey.extent(layout),
      zoom,
      spatialKey.col, // positionX
      if(invertY) Math.pow(2, zoom).toInt - 1 - spatialKey.row else spatialKey.row, // positionY
      cornerPoints)
  }

  /**
    * Write this tile to the given export directory. This generates a
    * directory structure that can be served in order to serve quantized mesh
    * in Cesium applications.
    * @param exportDir The directory to write this tile tree into.
    * @return True if writing was successful, false otherwise.
    */
  def write(exportDir: File): Boolean = {

    // Generate all required directories and files to write this tile.
    val tileDir = new File(exportDir.getAbsolutePath + "/" + zoomLevel + "/" + positionX)
    tileDir.mkdirs
    val terrainFile = new File(tileDir, positionY + ".terrain")
    terrainFile.createNewFile()

    true // Success
  }

}